Pick your favorite PHP framework - Zend Framework/Symfony is prefered, but not necessary needed.
DB storage engine is MySQL/MariaDB.
Consider (virtually) 50k rows in each table, you will create. For testing sake just few is enough.
Do not bother with any graphics. Simple HTML presentation with tables is enough.

Task:

1) Imagine ehop selling books (very common "howto" task)
2) You have to create filtered list of orders with pagination and some action
3) create DB with some dummy data about orders and books (books storage is to be used as e-shop datasource, orders storage is to be used for administration purposes)
4) consider more books per order (1...n) and each item could be either 1 piece or more pieces
5) Order list line (overview) contains: order ID, name of recipient, number of emails sent, total amount - price, list of ordered book codes, total items, order status (new, sent, processed)
6) pagination of orders - 2 items per page
7) filtering by Book code, recipient name, book ID and number if items (summary per order)
8) open order detail by click to the order ID and show table with book ID, book name, book price and ho many of each item is ordered
9) each line of list have checkbox for batch action (two batch starter buttons above the list): "Mark as sent", "Mark as processed"
10) you can check some orders and start one of those two batch actions
11) both actions will change order status accordingly
12) During both actions email to Recipient is sent. Emails have different body, different subject and are logged to the order (displaying in the order is not required, just storing them into DB)
13) Second type/mail will have one file attached
