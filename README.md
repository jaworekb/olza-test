## How to start
1. clone project
2. add domain olza-test.local to /etc/hosts
3. start docker containers
   `make up`
4. create db schema
   `make migrate`
5. generate example data
   `make load-fixtures`
6. http://olza-test.local should work

## Solution description

I modified a little the task. Instead of creating HTML, I created API. I hope it's not a problem.

- api documentation is available at this address http://olza-test.local/api/doc
- api documentation in json is in project - file api.json in main directory
- endpoint that returns list of orders: http://olza-test.local/api/orders
- filters and paging work: http://olza-test.local/api/orders?book_code=1&recipient_name=John%20Doe&book_id=1&number_of_items=1&page=1&items_per_page=2
- single order are available here: http://olza-test.local/api/orders/1
- batch actions also works:  http://olza-test.local/api/orders/processed and http://olza-test.local/api/orders/sent
   