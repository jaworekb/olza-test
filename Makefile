DOCKER_COMPOSE = docker-compose

up:
	$(DOCKER_COMPOSE) up -d

migrate:
	$(DOCKER_COMPOSE) exec php bin/console doctrine:migrations:migrate

load-fixtures:
	$(DOCKER_COMPOSE) exec php bin/console doctrine:fixtures:load -q

# all is phony...
.PHONY: %
.DEFAULT_GOAL =  up
