<?php

namespace App\Command;

class MarkOrderProcessed
{
    public int $orderId;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
    }
}