<?php

namespace App\Command;

class MarkOrderSent
{
    public int $orderId;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
    }
}