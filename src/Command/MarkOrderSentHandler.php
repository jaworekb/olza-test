<?php

namespace App\Command;

use App\Email\Mailer;
use App\MessageHandler\CommandHandlerInterface;
use App\Repository\OrderRepository;
use InvalidArgumentException;

class MarkOrderSentHandler implements CommandHandlerInterface
{
    private OrderRepository $orderRepository;
    private Mailer $mailer;

    public function __construct(OrderRepository $orderRepository, Mailer $mailer)
    {
        $this->orderRepository = $orderRepository;
        $this->mailer = $mailer;
    }

    public function __invoke(MarkOrderSent $markOrderSent)
    {
        $order = $this->orderRepository->findOne($markOrderSent->orderId);

        if (empty($order)) {
            throw new InvalidArgumentException('Given order id doesn\'t exist');
        }

        if($order->isSent()){
            throw new InvalidArgumentException('Given order has been already sent');
        }

        $email = $this->mailer->sendOrderSentEmail($order->getRecipient());
        $order->addOrderEmail($email->getFrom()[0]->toString(), $email->getTo()[0]->toString(), $email->getSubject(), $email->getTextBody());

        $order->sentStatusSent();
    }
}