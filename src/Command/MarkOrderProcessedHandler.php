<?php

namespace App\Command;

use App\Email\Mailer;
use App\MessageHandler\CommandHandlerInterface;
use App\Repository\OrderRepository;
use InvalidArgumentException;

class MarkOrderProcessedHandler implements CommandHandlerInterface
{
    private OrderRepository $orderRepository;
    private Mailer $mailer;

    public function __construct(OrderRepository $orderRepository, Mailer $mailer)
    {
        $this->orderRepository = $orderRepository;
        $this->mailer = $mailer;
    }

    public function __invoke(MarkOrderProcessed $markOrderProcessed)
    {
        $order = $this->orderRepository->findOne($markOrderProcessed->orderId);

        if (empty($order)) {
            throw new InvalidArgumentException('Given order id doesn\'t exist');
        }

        if ($order->isProcessed()) {
            throw new InvalidArgumentException('Given order has been already processed');
        }

        $email = $this->mailer->sendOrderProcessedEmail($order->getRecipient());
        $order->addOrderEmail($email->getFrom()[0]->toString(), $email->getTo()[0]->toString(), $email->getSubject(), $email->getTextBody());

        $order->setStatusProcessed();
    }
}