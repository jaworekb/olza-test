<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findOne($id): ?Order
    {
        $qb = $this->createQueryBuilder('o')
            ->addSelect(['r', 'ol', 'b', 'oe'])
            ->leftJoin('o.recipient', 'r')
            ->leftJoin('o.orderEmails', 'oe')
            ->leftJoin('o.orderLines', 'ol')
            ->leftJoin('ol.book', 'b')
            ->andWhere('o.id = ?1')
            ->setParameter(1, $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function list(
        int     $page,
        int     $itemsPerPage,
        ?string $filterBookCode = null,
        ?string $filterRecipientName = null,
        ?string $filterBookId = null,
        ?string $filterNumberOfItems = null
    )
    {
        $qb = $this->createQueryBuilder('o')
            ->addSelect(['r', 'ol', 'b', 'oe'])
            ->leftJoin('o.recipient', 'r')
            ->leftJoin('o.orderEmails', 'oe')
            ->leftJoin('o.orderLines', 'ol')
            ->leftJoin('ol.book', 'b')
            ->setMaxResults($itemsPerPage)
            ->setFirstResult(($page - 1) * $itemsPerPage)
            ->orderBy('o.id');

        if (!empty($filterBookCode)) {
            $qb->andWhere("EXISTS (SELECT ol2.id FROM App\Entity\OrderLine ol2 JOIN ol2.book b2 WITH b2.code = ?1 WHERE ol2.order = o.id)");
            $qb->setParameter(1, $filterBookCode);
        }

        if (!empty($filterRecipientName)) {
            $qb->andWhere('EXISTS (SELECT r2.id FROM App\Entity\Recipient r2 WHERE r2.id = o.recipient AND r2.name = ?2)');
            $qb->setParameter(2, $filterRecipientName);
        }

        if (!empty($filterBookId)) {
            $qb->andWhere("EXISTS (SELECT ol3.id FROM App\Entity\OrderLine ol3 JOIN ol3.book b3 WITH b3.id = ?3 WHERE ol3.order = o.id)");
            $qb->setParameter(3, $filterBookId);
        }

        if (!empty($filterNumberOfItems)) {
            $qb->andWhere("SIZE (o.orderLines) = ?4");
            $qb->setParameter(4, $filterNumberOfItems);
        }

        return new Paginator($qb, true);
    }

}