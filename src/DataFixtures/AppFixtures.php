<?php

namespace App\DataFixtures;

use App\Command\MarkOrderProcessed;
use App\Command\MarkOrderSent;
use App\Entity\Book;
use App\Entity\Order;
use App\Entity\Recipient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Messenger\MessageBusInterface;

class AppFixtures extends Fixture
{
    private Generator $faker;
    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    public function load(ObjectManager $manager): void
    {
        $this->faker = Factory::create();

        $books = [];
        for ($i = 1; $i <= 20; $i++) {
            $books[$i] = new Book($this->faker->sentence(4), $this->faker->isbn10, $this->faker->numberBetween(1000, 10000));
            $manager->persist($books[$i]);
        }

        $manager->flush();

        $recipients = [];
        for ($i = 1; $i <= 10; $i++) {
            $recipients[$i] = new Recipient($this->faker->name, $this->faker->email);
            $manager->persist($recipients[$i]);
        }

        $manager->flush();

        $orders = [];
        for ($i = 1; $i <= 10; $i++) {
            $order = new Order($this->faker->randomElement($recipients));

            for ($j = 1; $j <= $this->faker->numberBetween(1, 3); $j++) {
                $order->addOrderLine($this->faker->randomElement($books), $this->faker->numberBetween(1, 5));
            }

            $manager->persist($order);
            $manager->flush();

            if ($this->faker->boolean) {
                $this->bus->dispatch(new MarkOrderProcessed($order->getId()));

                if ($this->faker->boolean) {
                    $this->bus->dispatch(new MarkOrderSent($order->getId()));
                }
            }

            $orders[$i] = $order;
        }


        $manager->flush();
    }
}
