<?php

namespace App\Controller;

use App\Command\MarkOrderProcessed;
use App\Command\MarkOrderSent;
use App\Entity\Order;
use App\Query\ListOrdersQuery;
use App\Repository\OrderRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrderController extends AbstractController
{
    private SerializerInterface $serializer;
    private MessageBusInterface $queryBus;
    private OrderRepository $orderRepository;
    private MessageBusInterface $commandBus;

    public function __construct(SerializerInterface $serializer, MessageBusInterface $queryBus, OrderRepository $orderRepository, MessageBusInterface $commandBus)
    {
        $this->serializer = $serializer;
        $this->queryBus = $queryBus;
        $this->orderRepository = $orderRepository;
        $this->commandBus = $commandBus;
    }

    /**
     * List orders
     *
     * @Route("/api/orders", methods={"GET"}, name="order_list")
     *
     * @OA\Parameter(
     *     name="book_code",
     *     in="query",
     *     description="Filter by book code",
     *     @OA\Schema(type="string")
     * )
     *
     * @OA\Parameter(
     *     name="recipient_name",
     *     in="query",
     *     description="Filter by recipient name",
     *     @OA\Schema(type="string")
     * )
     *
     * @OA\Parameter(
     *     name="book_id",
     *     in="query",
     *     description="Filter by book id",
     *     @OA\Schema(type="int")
     * )
     *
     * @OA\Parameter(
     *     name="number_of_items",
     *     in="query",
     *     description="Filter by number of items",
     *     @OA\Schema(type="int")
     * )
     *
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page number",
     *     @OA\Schema(type="int")
     * )
     *
     * @OA\Parameter(
     *     name="items_per_page",
     *     in="query",
     *     description="Number of items on page",
     *     @OA\Schema(type="int")
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns list of orders",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Order::class, groups={"order_details"}))
     *     )
     * )
     *
     * @OA\Tag(name="Orders")
     */
    public function list(Request $request): Response
    {
        $envelope = $this->queryBus->dispatch(new ListOrdersQuery(
            $request->query->get('book_code'),
            $request->query->get('recipient_name'),
            $request->query->get('book_id'),
            $request->query->get('number_of_items'),
            $request->query->get('page', 1),
            $request->query->get('items_per_page', 2),
        ));

        $handled = $envelope->last(HandledStamp::class);
        $orders = $handled->getResult();

        return $this->json($orders, 200, [], ['groups' => 'order_details']);
    }

    /**
     * Get details of one order
     *
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Order Id",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     *
     * @Route("/api/orders/{id}", methods={"GET"}, name="order_one")
     *
     * @OA\Tag(name="Orders")
     */
    public function one(int $id): Response
    {
        $order = $this->orderRepository->findOne($id);

        return $this->json($order, 200, [], ['groups' => 'order_details']);
    }

    /**
     * Batch action - mark orders as processed
     *
     * @OA\RequestBody(
     *     description="Array of order id's to mark as processed",
     *     required=true,
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(@OA\Schema(type="string"))
     *     )
     * )
     *
     * @Route("/api/orders/processed", methods={"PATCH"}, name="order_mark_as_processed")
     *
     * @OA\Tag(name="Orders")
     */
    public function markAsProcessed(Request $request): Response
    {
        $orderIds = $request->toArray();

        foreach ($orderIds as $orderId) {
            $this->commandBus->dispatch(new MarkOrderProcessed($orderId));
        }

        return $this->json([]);
    }

    /**
     * Batch action - mark orders as sent
     *
     * @OA\RequestBody(
     *     description="Array of order id's to mark as sent",
     *     required=true,
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(@OA\Schema(type="string"))
     *     )
     * )
     *
     * @Route("/api/orders/sent", methods={"PATCH"}, name="order_mark_as_sent")
     *
     * @OA\Tag(name="Orders")
     */
    public function markAsSent(Request $request): Response
    {
        $orderIds = $request->toArray();

        foreach ($orderIds as $orderId) {
            $this->commandBus->dispatch(new MarkOrderSent($orderId));
        }

        return $this->json([]);
    }
}