<?php

namespace App\Query;

class ListOrdersQuery
{

    public ?string $filterBookCode;
    public ?string $filterRecipientName;
    public ?string $filterBookId;
    public ?string $filterNumberOfItems;
    public int $limitPage;
    public int $limitItemsPerPage;

    public function __construct(
        ?string $filterBookCode = null,
        ?string $filterRecipientName = null,
        ?string $filterBookId = null,
        ?string $filterNumberOfItems = null,
        int     $limitPage = 1,
        int     $limitItemsPerPage = 2
    )
    {
        $this->filterBookCode = $filterBookCode;
        $this->filterRecipientName = $filterRecipientName;
        $this->filterBookId = $filterBookId;
        $this->filterNumberOfItems = $filterNumberOfItems;
        $this->limitPage = $limitPage;
        $this->limitItemsPerPage = $limitItemsPerPage;
    }
}