<?php

namespace App\Query;

use App\MessageHandler\QueryHandlerInterface;
use App\Repository\OrderRepository;

class ListOrdersQueryHandler implements QueryHandlerInterface
{
    private OrderRepository $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function __invoke(ListOrdersQuery $listOrdersQuery)
    {
        return $this->orderRepository->list(
            $listOrdersQuery->limitPage,
            $listOrdersQuery->limitItemsPerPage,
            $listOrdersQuery->filterBookCode,
            $listOrdersQuery->filterRecipientName,
            $listOrdersQuery->filterBookId,
            $listOrdersQuery->filterNumberOfItems
        );
    }
}