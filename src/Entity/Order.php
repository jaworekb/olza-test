<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Mime\Email;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity
 * @ORM\Table(name="`order`")
 */
class Order
{
    const STATUS_NEW = 1;
    const STATUS_PROCESSED = 2;
    const STATUS_SENT = 3;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @Groups({"order_details"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"order_details"})
     */
    private int $status;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"order_details"})
     */
    private int $priceTotal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Recipient")
     * @ORM\JoinColumn(name="recipient_id", referencedColumnName="id")
     * @Groups({"order_details"})
     */
    private Recipient $recipient;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderLine", mappedBy="order", cascade={"persist", "remove"})
     * @Groups({"order_details"})
     * @var OrderLine[]|ArrayCollection
     */
    private $orderLines;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderEmail", mappedBy="order", cascade={"persist", "remove"})
     * @var OrderEmail[]|ArrayCollection
     */
    private $orderEmails;

    public function __construct(Recipient $recipient)
    {
        $this->recipient = $recipient;
        $this->status = self::STATUS_NEW;
        $this->orderLines = new ArrayCollection();
        $this->orderEmails = new ArrayCollection();
        $this->priceTotal = 0;
    }

    public function addOrderLine(Book $book, int $numberOfItems)
    {
        $this->orderLines->add(new OrderLine($book, $numberOfItems, $this));
        $this->priceTotal += $book->getPrice() * $numberOfItems;
    }

    public function setStatusProcessed()
    {
        $this->status = self::STATUS_PROCESSED;
    }

    public function sentStatusSent()
    {
        $this->status = self::STATUS_SENT;
    }

    public function isSent()
    {
        return $this->status === self::STATUS_SENT;
    }

    public function isProcessed()
    {
        return $this->status === self::STATUS_PROCESSED;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getPriceTotal(): int
    {
        return $this->priceTotal;
    }

    public function getRecipient(): Recipient
    {
        return $this->recipient;
    }

    /**
     * @return OrderLine[]|ArrayCollection
     */
    public function getOrderLines()
    {
        return $this->orderLines;
    }

    /**
     * @return OrderEmail[]|ArrayCollection
     */
    public function getOrderEmails()
    {
        return $this->orderEmails;
    }

    /**
     * @Groups({"order_details"})
     */
    public function getNumberOfOrderEmails(): int
    {
        return $this->orderEmails->count();
    }

    public function addOrderEmail(string $from, string $to, string $subject, string $body)
    {
        $this->orderEmails->add(new OrderEmail($from, $to, $subject, $body, $this));
    }
}