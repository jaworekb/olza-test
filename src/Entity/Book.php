<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="book")
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @Groups({"order_details"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string")
     * @Groups({"order_details"})
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     * @Groups({"order_details"})
     */
    private string $code;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"order_details"})
     */
    private int $price;

    public function __construct(string $name, string $code, int $price)
    {
        $this->name = $name;
        $this->code = $code;
        $this->price = $price;
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getPrice(): int
    {
        return $this->price;
    }


}