<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="order_email")
 */
class OrderEmail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", name="`from`")
     */
    private string $from;

    /**
     * @ORM\Column(type="string", name="`to`")
     */
    private string $to;

    /**
     * @ORM\Column(type="string")
     */
    private string $subject;

    /**
     * @ORM\Column(type="string")
     */
    private string $body;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $sentAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="orderEmails")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private Order $order;

    public function __construct(string $from, string $to, string $subject, string $body, Order $order)
    {
        $this->from = $from;
        $this->to = $to;
        $this->subject = $subject;
        $this->body = $body;
        $this->order = $order;
        $this->sentAt = new DateTime();
    }


}