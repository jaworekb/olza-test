<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="order_line")
 */
class OrderLine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"order_details"})
     */
    private int $itemPrice;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"order_details"})
     */
    private int $numberOfItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="orderLines")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private Order $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Book")
     * @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     * @Groups({"order_details"})
     */
    private Book $book;

    public function __construct(Book $book, int $numberOfItems, $order)
    {
        $this->book = $book;
        $this->numberOfItems = $numberOfItems;
        $this->order = $order;
        $this->itemPrice = $book->getPrice();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItemPrice(): int
    {
        return $this->itemPrice;
    }

    public function getNumberOfItems(): int
    {
        return $this->numberOfItems;
    }

    public function getBook(): Book
    {
        return $this->book;
    }


}