<?php

namespace App\Email;

use App\Entity\Recipient;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class Mailer
{
    private MailerInterface $mailer;
    private string $attachmentPath;

    public function __construct(MailerInterface $mailer, string $attachmentPath)
    {
        $this->mailer = $mailer;
        $this->attachmentPath = $attachmentPath;
    }

    public function sendOrderProcessedEmail(Recipient $recipient): Email
    {
        $email = (new Email())
            ->from('app@example.com')
            ->to($recipient->getEmail())
            ->subject('Order processed')
            ->text('xxx');

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $exception) {
            //todo handle error while sending
        }

        return $email;
    }

    public function sendOrderSentEmail(Recipient $recipient)
    {
        $email = (new Email())
            ->from('app@example.com')
            ->to($recipient->getEmail())
            ->subject('Order sent')
            ->text('xxx')
            ->attachFromPath($this->attachmentPath . '/empty.pdf');

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $exception) {
            //todo handle error while sending
        }

        return $email;
    }
}